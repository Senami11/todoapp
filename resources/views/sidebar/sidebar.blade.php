<div class="leftside-menu">
    
    <!-- LOGO -->
    <a href="index.html" class="logo text-center logo-light">
        <span  class="logo-lg">
            <img src="assets/images/LOGO2.png" alt="" height="66">
        </span>
        <span class="logo-sm">
            <img src="assets/images/LOGO2.png" alt="" height="26">
        </span>
    </a>

    <!-- LOGO -->
    <a href="index.html" class="logo text-center logo-dark">
        <span class="logo-lg">
            <img src="assets/images/logo-dark.png" alt="" height="16">
        </span>
        <span class="logo-sm">
            <img src="assets/images/logo_sm_dark.png" alt="" height="16">
        </span>
    </a>


    <div class="h-100" id="leftside-menu-container" data-simplebar="">

        <!--- Sidemenu -->
        <ul class="side-nav">
            <li class="side-nav-item">
                <a href="{{route('dashbordadmin')}}" aria-expanded="false" aria-controls="sidebarDashboards" class="side-nav-link">
                    <i class="uil-home-alt"></i>
                    <span> Dashboards </span>
                </a>
                
            </li>
            <li class="side-nav-item">
                <a  href="{{route('consult.element')}}" aria-expanded="false" aria-controls="sidebarTasks" class="side-nav-link">
                    <i class="uil-clipboard-alt"></i>
                    <span> Régistre </span>
                </a>
               
            </li>
              <li class="side-nav-item">
                <a  href="{{route('create.element')}}" aria-expanded="false" aria-controls="sidebarForms" class="side-nav-link">
                    <i class="uil-document-layout-center"></i>
                    <span> Enrégistrer </span>
                </a>
              
            </li> 
            <!-- <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarTables" aria-expanded="false" aria-controls="sidebarTables" class="side-nav-link">
                    <i class="uil-table"></i>
                    <span> Modification </span>
                </a>
                
            </li>
            <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarTasks" aria-expanded="false" aria-controls="sidebarTasks" class="side-nav-link">
                    <i class="uil-clipboard-alt"></i>
                    <span> Récupération </span>
                </a>
                
            </li>  -->
            <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarTasks" aria-expanded="false" aria-controls="sidebarTasks" class="side-nav-link">
                    <i class="uil-user"></i>
                    <span>Personnel</span>
                    <span class="menu-arrow"></span>
                </a>
                <div class="collapse" id="sidebarTasks">
                    <ul class="side-nav-second-level">
                        <li>
                            <a href="{{route('user.list')}}">Liste</a>
                        </li>
                        <li>
                            <a href="{{route('createsm.admin')}}">Ajouter personnel</a>
                        </li>
                        <li>
                            <a href="{{route('choix.personnel')}}">Ajouter utilisateur</a>
                        </li>
                       
                    </ul>
                </div>
            </li>
            
            <!-- <li class="side-nav-item">
                <a href="apps-chat.html" class="side-nav-link">
                    <i class="uil-comments-alt"></i>
                    <span class="badge bg-success float-end">0</span>
                    <span> Discussion</span>
                </a>
            </li> -->

        </ul>

        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <script>document.write(new Date().getFullYear())</script> © PREGISTRE - ortb.com
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-end footer-links d-none d-md-block">
                                    <a href="javascript: void(0);">A propos</a>
                                    <a href="javascript: void(0);">Nous suivre</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>